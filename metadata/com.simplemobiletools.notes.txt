Categories:System
License:Apache2
Web Site:
Source Code:https://github.com/SimpleMobileTools/Simple-Notes
Issue Tracker:https://github.com/SimpleMobileTools/Simple-Notes/issues
Changelog:https://github.com/SimpleMobileTools/Simple-Notes/blob/HEAD/CHANGELOG.md

Auto Name:Simple Notes
Summary:A textfield for quick notes with a customizable widget
Description:
Need to take a quick note of something to buy, an address, or a startup idea?
Then this is the app you've been looking for! You can access the note in no time
by using the customizable and resizable widget, which opens the app on click.
.

Repo Type:git
Repo:https://github.com/SimpleMobileTools/Simple-Notes

Build:1.6,6
    commit=75b23ffd313a542175802328187996b5f0a826c9
    subdir=app
    gradle=yes

Build:1.7,7
    commit=1.7
    subdir=app
    gradle=yes

Build:1.8,8
    commit=1.8
    subdir=app
    gradle=yes

Build:1.9,9
    commit=1.9
    subdir=app
    gradle=yes

Build:1.10,10
    commit=1.10
    subdir=app
    gradle=yes

Build:1.11,11
    commit=1.11
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.11
Current Version Code:11
